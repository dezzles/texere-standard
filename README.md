# README #

This document specifies how Texere compatible libraries should communicate with systems

## Contents ##

* Logging
* Communication

## Logging ##

All logs should be stored as a JSON object. Components of the JSON object are defined below

### Context ###

All logs should a context object that contains `tex-trace-id`, `tex-span-id` and `tex-parent-id` as below.
The following values should also found on the root of the JSON object

| Field | Description | Example |
| ----- | ----- | ----- |
| level | logging level | INFO |
| message | Location for log messages to be stored | Received GET request on / |
| timestamp | Unix timestamp for the event | 1616213786822 |
| service | Name of the service that is logging | swamp_emu |
| hostname | Host that the service or application is running on | i-342j3k23jh4 |
| pid | Proccess id of the application | 432 |
| thread | The thread Id the task is running on. If no thread is available, return -1 | -1 |
| event | Description of the task that is in progress | startup |

#### Example ####

```
{
  "context": {
    "tex-trace-id": "a8789ba8-6803-49e8-aaf4-6b52689a433b",
	"tex-span-id": "ce287297-dd1f-4db6-a62e-d5bfbb4ae16a",
	"tex-parent-id": null
  },
  "message": "Received GET response on /login",
  "timestamp": 1616213786822
  "service": "swamp_emu",
  "hostname": "i-32423423jfkdsaj",
  "pid" 432,
  "thread" 869,
  "event": "login"
}
```

## Messaging ##

### Event Message ###

Messages should contain the following fields
| Field | Description | Example |
| ----- | ----- | ----- |
| context | Context information as per standars | |
| eventType | Text descriptor of the type of the event | USER_LOGIN |
| eventData | Data contained by the event | See below example |

#### Example ####

```
{
  "context": {
    "tex-trace-id": "a8789ba8-6803-49e8-aaf4-6b52689a433b",
	"tex-span-id": "ce287297-dd1f-4db6-a62e-d5bfbb4ae16a",
	"tex-parent-id": null
  },
  "eventType": "USER_LOGIN",
  "eventData": {
    "userId": 7742
  }
}
```

### Headers ###

Standard communication headers are as follows

| Header | Description |
| ----- | ----- |
| tex-trace-id | End to end id for a request. Should be maintained for a single request across multiple services |
| tex-span-id | Id maintained for a request inside a single service |
| tex-parent-id | tex-span-id that is passed in from another service. Used for tracking where a request initiated |
| tex-user-id | Optional user id that is set when a user is logged in |